import Vue from 'vue'
import { firestorePlugin } from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.use(firestorePlugin)
if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyBC6oJattNdYnsrGo6Lg8ugv3JcTEr3l4s",
    authDomain: "metts-4bc54.firebaseapp.com",
    databaseURL: "https://metts-4bc54.firebaseio.com",
    projectId: "metts-4bc54",
    storageBucket: "metts-4bc54.appspot.com",
    messagingSenderId: "218047028798",
    appId: "1:218047028798:web:f0f52fb385082eb1457b6f",
    measurementId: "G-RPZGH8JC7W"
  })
}

export const db = firebase.firestore()
