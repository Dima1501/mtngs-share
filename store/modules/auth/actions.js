import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

import axios from '~/plugins/axios'

const authStateChange = ({commit, dispatch, store} , registrationData) => {
  firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        commit('auth/addUserData', user, {root:true})
        dispatch("meetings/loadMyMeetings", null, {root:true})
      } else {
        commit('auth/addUserData', '', {root:true})
      }
  })
}

const logOut = ({commit, dispatch, store} , registrationData) => {
  firebase.auth().signOut().then(function() {
    // console.log('success log out')
  }).catch(function(error) {
    console.log(error)
  })
}

const registration = ({commit, dispatch, store} , registrationData) => {
    // commit('view/togglePopupLoading', null, {root:true})
    firebase.auth().createUserWithEmailAndPassword(registrationData.email, registrationData.password)
      .then((user) => {
        // commit('view/togglePopupLoading', null, {root:true})
        firebase.auth().currentUser.updateProfile({
          displayName: registrationData.name
        }).then(function() {
          commit('view/togglePopup', null, {root:true})
          firebase.firestore().collection('users').add({
            uid: user.user.uid,
            userName: user.user.displayName,
            timestamp: 0 - user.user.metadata.a
          })
          const emailData = {
            email: user.email,
            name: user.displayName,
            message: `Здравствуй, ${user.displayName}!`,
            text: `Спасибо за регистрацию на <a href='https://mtngs.online'>mtngs.online</a>`
          }
          this.$store.dispatch('auth/sendMessage', emailData)
        }).catch(function(error) {
          console.log(error)
        })
      }).catch(function(error) {
        console.log(error)
        // commit('view/togglePopupLoading', null, {root:true})
        dispatch('view/showNotify', error.message, {root:true})
    })
  }

const restorePassword = ({dispatch, commit, state} , reset) => {
    firebase.auth().sendPasswordResetEmail(reset.email).then(function() {
        let message = 'We have sent message to ' + reset.email
      dispatch('view/showNotify', message, {root:true})
    //   commit('view/togglePopupLoading', null, {root:true})
        // console.log('success')
    }).catch(function(error) {
        console.log(error.message)
      dispatch('view/showNotify', error.message, {root:true})
    })
}

const signIn = ({commit, dispatch, store}, signInData) => {
    // commit('view/togglePopupLoading', null, {root:true})
    firebase.auth().signInWithEmailAndPassword(signInData.email, signInData.password)
      .then(() => {
        commit('view/togglePopup', null, {root:true})
        // console.log('success')
        // commit('view/togglePopupLoading', null, {root:true})
      }).catch(function(error) {
            console.log(error)
        // commit('view/togglePopupLoading', null, {root:true})
      dispatch('view/showNotify', error.message, {root:true})
    })
}

const sendMessage = async({state, commit}, payload) => {

  let emailInfo = payload
  let emailProvider = {
    service: 'gmail',
    username: '000',
    password: '000'
  }

  if (emailProvider.username !== '' && emailProvider.password !== '') {
    try {
      const { res } = await axios.post('/api/nodemailer', {
        emailInfo,
        emailProvider
      })
    } catch (e) {
      console.log(e)
      }
  }
}

export default {
    restorePassword,
    signIn,
    registration,
    authStateChange,
    logOut,
    sendMessage
}