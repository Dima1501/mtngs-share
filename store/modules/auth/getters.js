const getUser = (state) => {
    return state.user
}

const getAuthData = (state) => {
    return state
}

export default {
    getUser,
    getAuthData
}