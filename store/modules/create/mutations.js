const addReqLine = (state) => {
    state.reqsLen += 1
    state.reqs.push('')
}

const removeReqLine = (state) => {
    if (state.reqsLen > 1) {
        state.reqsLen -= 1
        state.reqs.pop()
    }
}

export default {
    addReqLine,
    removeReqLine
}