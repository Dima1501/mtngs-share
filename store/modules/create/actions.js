import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const editMeeting = ({commit, dispatch, rootState} , data) => {
  firebase.firestore().collection('meetings').doc(data.id).update({
    status: 'pending'
  })
  firebase.firestore().collection('moderate').doc(data.id).update({
    title: data.title,
    subtitle: data.subtitle,
    reqs: data.reqs,
    reqsLen: data.reqs.length,
    status: data.status
  }).then(() => {
    dispatch('view/showNotify', 'Изменения были отправлены на модерацию', {root:true})

    const emailData = {
      email: rootState.auth.user.email,
      name: rootState.auth.user.displayName,
      message: `Здравствуй, ${rootState.auth.user.displayName}!`,
      text: `Изменения в митинге были отправлены на модерацию, мы уведомим вас, как только все проверим`
    }
    dispatch('auth/sendMessage', emailData, {root:true}) 

    const emailDataAdmins = {
      email: 'mtngsonline@yandex.ru',
      name: rootState.auth.user.displayName,
      message: `Внесены изменения в митинг`,
      text: ``
    }

    let timeout = setTimeout(() => {
      dispatch('auth/sendMessage', emailDataAdmins, {root:true})
      clearTimeout(timeout)
    }, 5000);

  })
  .catch(err => console.log(err))
}

const createMeeting = ({commit, dispatch, rootState} , data) => {
  
    firebase.firestore().collection('moderate').add({
      coords: data.coords,
      title: data.title,
      subtitle: data.subtitle,
      reqs: data.reqs,
      reqsLen: data.reqs.length,
      author: data.author,
      authorName: data.authorName,
      status: 'pending',
      id: data.id,
      peoples: 1,
      participants: [rootState.auth.user.uid]
    }).then((e) => {
      firebase.firestore().collection('moderate').doc(e.id).update({
        id: e.id
      })
  
      firebase.firestore()
      .collection('meetings')
      .doc(e.id)
      .set(
        { id: e.id, coords: data.coords, status: 'pending' }
      ).then(() => {})
  
      commit('view/togglePopup', null, {root:true})
      dispatch('view/showNotify', 'Митинг был создан и отправлен на модерацию. Вы можете отслеживать статус в поле "Мои митинги"', {root:true})
      
      // Send message to user
      const emailData = {
        email: rootState.auth.user.email,
        name: rootState.auth.user.displayName,
        message: `Здравствуй, ${rootState.auth.user.displayName}!`,
        text: `Новый митинг был создан и отправлен на модерацию, мы уведомим вас, как только все проверим`
      }
      dispatch('auth/sendMessage', emailData, {root:true}) 

      // Send message to admins
      const emailDataAdmins = {
        email: 'mtngsonline@yandex.ru',
        name: rootState.auth.user.displayName,
        message: `Создан новый митинг от ${rootState.auth.user.displayName}!`,
        text: ``
      }

      let timeout = setTimeout(() => {
        dispatch('auth/sendMessage', emailDataAdmins, {root:true})
        clearTimeout(timeout)
      }, 5000);

      commit('view/toggleCreate', false, {root:true})
      commit('map/toggleMarker', false, {root:true})

      dispatch('meetings/loadMyMeetings', null, {root:true})
  
      firebase.firestore().collection('chat').add({
        id: e.id,
        messages: []
      }).then((chat) => {
        firebase.firestore().collection('moderate').doc(e.id).update({
          chatId: chat.id
        }).then(() => console.log('fin'))
      })
    }).catch((err) => {
      console.log(err)
    })
}

export default {
  createMeeting,
  editMeeting
}