const togglePopup = (store, popup) => {
  store.commit('togglePopup', popup)
}

const toggleSidebar = (store, type) => {
    store.commit('toggleSidebar', type)
}

const showNotify = (store, message) => {
    store.commit('toggleNotify', message)

    let timeout = setTimeout(() => {
        store.commit('toggleNotify', message)
        clearTimeout(timeout)
    },5000)
}

export default {
    togglePopup,
    toggleSidebar,
    showNotify
}