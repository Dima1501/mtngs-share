const getActivePopup = (state) => {
    return state.popups.activePopup
}

const getSidebarType = (state) => {
    return state.sidebar.type
}

export default {
    getActivePopup,
    getSidebarType
}