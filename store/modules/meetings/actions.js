import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const loadMyMeetings = ({commit, dispatch, rootState}, data) => {
    let author = rootState.auth.user.uid
    
    if (author) {
        commit('meetings/clearMyMeeting', null, {root:true})
        commit('view/togglePreloader', 'myMeetings', {root: true})
        dispatch('loadMapMeetings', null)
    
        firebase.firestore().collection('moderate').where('author', '==', author).get().then((snapshot) => {
            snapshot.forEach((doc) => {
                let data = {
                    fields: doc.data(),
                    id: doc.id
                }
                data.fields.id = doc.id
                commit('meetings/writeMyMeeting', data, {root:true})
            })
        }).then(() => {
            commit('view/togglePreloader', 'myMeetings', {root: true})
        })
    }
}

const deleteMeeting = ({commit, dispatch, rootState}, id) => {
    commit('view/togglePreloader', 'myMeetings', {root: true})
    firebase.firestore().collection('moderate').doc(id).delete()
    firebase.firestore().collection('meetings').doc(id).delete()
    .then(() => {
        dispatch('loadMyMeetings', null)
        dispatch('loadMapMeetings', null)
    }).then(() => {
        commit('view/togglePreloader', 'myMeetings', {root: true})
    }).catch((err) => console.log(err))
}

const loadModerate = ({commit, dispatch, rootState}, status) => {
    commit('meetings/clearModerateMeeting', null, {root:true})
    firebase.firestore().collection('moderate').where('status', '==', status).get().then((snapshot) => {
        snapshot.forEach((doc) => {
            let data = {
                fields: doc.data(),
                id: doc.id
            }
            data.fields.id = doc.id
            commit('meetings/setModerateMeetings', data, {root:true})
        })
    }).then(() => {
        // console.log('loaded your meetings')
    })
}

const moderateMeeting = ({commit, dispatch, rootState}, data) => {
    firebase.firestore().collection('meetings').doc(data.id).update({
        status: data.status
    })
    .catch((err) => console.log(err))
    
    firebase.firestore().collection('moderate').doc(data.id).update({
        status: data.status
    })
    .then(() => {
        console.log(data)
        dispatch('loadModerate', 'pending')

        if (data.status == 'rejected') {
            const emailData = {
                email: rootState.auth.user.email,
                name: rootState.auth.user.displayName,
                message: `Здравствуй, ${rootState.auth.user.displayName}!`,
                text: `Митинг был отклонен, его можно отредактировать и отправить заново в меню "Мои митинги" на <a href='https://mtngs.online'>mtngs.online</a>`
            }
            dispatch('auth/sendMessage', emailData, {root:true}) 
        } else if (data.status == 'public') {
            const emailData = {
                email: rootState.auth.user.email,
                name: rootState.auth.user.displayName,
                message: `Здравствуй, ${rootState.auth.user.displayName}!`,
                text: `Митинг был успешно опубликован на <a href='https://mtngs.online'>mtngs.online</a> и доступен по ссылке <a href='https://mtngs.online/#${data.id}'>https://mtngs.online/#${data.id}</a>`
            }
            dispatch('auth/sendMessage', emailData, {root:true}) 
        }
    })
    .catch((err) => console.log(err))
}

const loadPopupData = ({commit, dispatch, rootState}, data) => {
    commit('meetings/setPopupMeeting', {}, {root:true})
    firebase.firestore().collection('moderate').doc(data).get().then(snapshot => {
        commit('meetings/setPopupMeeting', snapshot.data(), {root:true})
    }).then(() => {
        firebase.firestore().collection('moderate').doc(data).onSnapshot(snapshot => {
            rootState.meetings.popupMeeting.peoples = snapshot.data().peoples
        })
    })
}

const loadMapMeetings = ({commit, dispatch, rootState}, data) => {

    commit('meetings/clearMapMeeting', {}, {root:true})

    firebase.firestore().collection('meetings').get().then(snapshot => {
        snapshot.forEach(doc => {
            let data = {
                fields: doc.data(),
                id: doc.id
            }
            commit('meetings/setMapMeetings', data, {root:true})
        })
    })
}

const openMeeting = ({commit, dispatch, rootState}, meeting) => {
    commit('view/toggleSidebar', 'preview', {root: true})
    commit('meetings/togglePreviewMeeting', meeting, {root: true})
}

const loadChat = ({commit, dispatch, rootState}, id) => {
    commit('view/toggleSidebar', 'chat', {root: true})
    commit('clearChat', null)

    firebase.firestore().collection('chat').where('id', '==', id).onSnapshot(snapshot => {
        snapshot.forEach((doc) => {
            if (rootState.meetings.previewMeeting.id == doc.data().id) {
                commit('writeChat', doc.data())
            }
        })
    })
}

const sentMessage = ({commit, dispatch, rootState}, data) => {
    commit('addMessage', data)
    firebase.firestore().collection('chat').doc(data.id).set({
        messages: rootState.meetings.chat.messages,
    }, {merge: true}).then(() => {
        // console.log('message has been sent')
    })
}

const supportMeeting = ({commit, dispatch, rootState}, data) => {
    firebase.firestore().collection('moderate').doc(data.id).update({
        peoples: rootState.meetings.previewMeeting.peoples + 1,
        participants: data.participants
    }).catch(err => console.log(err))
}

const exitMeeting = ({commit, dispatch, rootState}, data) => {
    firebase.firestore().collection('moderate').doc(data.id).update({
        peoples: rootState.meetings.previewMeeting.peoples - 1,
        participants: data.participants
    }).catch(err => console.log(err))
}

const checkHash = ({commit, dispatch, rootState}, data) => {
    if(window.location.hash) {
        let hash = window.location.hash.split('?')[0].substr(1);
        firebase.firestore().collection('moderate').doc(hash).get().then((snapshot) => {
            if (snapshot.exists) {
                commit('view/toggleSidebar', 'preview', {root: true})
                commit('meetings/togglePreviewMeeting', snapshot.data(), {root: true})
                rootState.map.map.panTo(snapshot.data().coords)
            } else {
                dispatch('view/showNotify', 'Митинг не найден, некорректная ссылка', {root:true})
            }
        }).then(() => {})
        .catch(err => console.log(err))
    }
}
  
export default {
    loadMyMeetings,
    deleteMeeting,
    loadModerate,
    moderateMeeting,
    loadMapMeetings,
    openMeeting,
    loadChat,
    sentMessage,
    supportMeeting,
    exitMeeting,
    checkHash,
    loadPopupData
}