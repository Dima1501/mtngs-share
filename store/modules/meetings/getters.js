const getPopupMeeting = (state) => {
    return state.popupMeeting
}

const getMapMeetings = (state) => {
    return state.mapMeetings
}

const getMyMeetings = (state) => {
    return state.myMeetings
}

const getEditMeeting = (state) => {
    return state.editMeeting
}

const getPreviewMeeting = (state) => {
    return state.previewMeeting
}

export default {
    getPopupMeeting,
    getMapMeetings,
    getMyMeetings,
    getEditMeeting,
    getPreviewMeeting
}