const writeMyMeeting = (state, data) => {
    state.myMeetings.push(data.fields)
}

const clearMyMeeting = (state) => {
    state.myMeetings = []
}

const clearModerateMeeting = (state) => {
    state.moderateMeetings = []
}

const clearMapMeeting = (state) => {
    state.mapMeetings = []
}

const setModerateMeetings = (state, data) => {
    state.moderateMeetings.push(data.fields)
}

const setMapMeetings = (state, data) => {
    state.mapMeetings.push(data.fields)
}

const togglePreviewMeeting = (state, meeting) => {
    state.previewMeeting = meeting
}

const writeChat = (state, chat) => {
    state.chat = chat
}

const clearChat = (state) => {
    state.chat = []
}

const addMessage = (state, data) => {
    state.chat.messages.push({
        author: data.author,
        message: data.message,
        authorId: data.authorId
    })
}

const setEditMeeting = (state, data) => {
    state.editMeeting = JSON.parse(JSON.stringify(data))
}

const setPopupMeeting = (state, data) => {
    state.popupMeeting = data
}

export default {
    writeMyMeeting,
    clearMyMeeting,
    setModerateMeetings,
    clearModerateMeeting,
    clearMapMeeting,
    setMapMeetings,
    togglePreviewMeeting,
    writeChat,
    clearChat,
    addMessage,
    setEditMeeting,
    setPopupMeeting
}