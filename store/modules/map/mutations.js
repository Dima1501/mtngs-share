const toggleMarker = (state, type) => {
    state.newMarker.visible = !state.newMarker.visible
}

const setBounds = (state, bounds) => {
    state.bounds = bounds
}

export default {
    toggleMarker,
    setBounds
}