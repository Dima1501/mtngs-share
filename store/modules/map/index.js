import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  center: [37.622893, 55.753555],
  newMarker: {
    coords: null,
    title: null,
    subtitle: null,
    reqs: [],
    visible: false
  },
  bounds: {}
};

export default {
  namespaced:   true,
  state,
  actions,
  mutations,
  getters
};