const getNewMarker = (state) => {
    return state.newMarker
}

const getMapState = (state) => {
    return state.map
}

const getMapBounds = (state) => {
    return state.bounds
}

export default {
    getNewMarker,
    getMapState,
    getMapBounds
}