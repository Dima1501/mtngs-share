import path from 'path'
import fs from 'fs'

module.exports = {
  // server: {
  //   https: {
  //     key: fs.readFileSync('./server.key', 'utf8'),
  //     cert: fs.readFileSync('./certificate.crt', 'utf8')
  //   },
  //   port: 443,
  //   host: 'mtngs.online'
  // },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Митинги Онлайн',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Привет, тут можно создать или присоединиться к онлайн митингу, а также поучаствовать в его обсуждении и поделиться ссылкой со знакомыми, чтобы обратить внимание общества на ту или иную проблему' },
      { name: 'yandex-verification', content: '3af64f1ebef26c13' }
    ],
    link: [
      { rel: 'stylesheet',  href: 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css'},
      { rel: 'apple-touch-icon', sizes: '180x180" href="/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png" sizes="32x32" href="/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png" sizes="16x16" href="/favicon-16x16.png' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#5bbad5' },
      { name: 'msapplication-TileColor', content: '#9f00a7' },
      { name: 'theme-color', content: '#ffffff' }
    ]
  },

  mode: 'spa',
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },

  plugins: [
    { src: '~/plugins/mapbox', mode: 'client' }
  ],

  buildModules: [
    ['@nuxtjs/vuetify', {}]
  ],

  modules: [
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '62916262',
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
      }
    ],
  ],

  serverMiddleware: [
    '~/api/nodemailer'
  ]
}

